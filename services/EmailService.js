var ejs = require('ejs');
var Configurator = require('../helpers/configurator');
var sendGrid = require("sendgrid")(Configurator.get('sendGrid').user, Configurator.get('sendGrid').password);
var exports = {};
var Promise = require('bluebird');
var templateFolder = './views/email/';

exports.sendEmail = function(options){
    return new Promise(function (resolve, reject) {
        ejs.renderFile(templateFolder+options.template+'.ejs', options, null, function(err, body){
            if(err){
                reject(err);
            } else{
                sendGrid.send({
                    to      : options.email,
                    from    : "noreply@cyclopstudio.com",
                    subject : options.subject,
                    html    : body
                }, function(err, json) {
                    if(err) {
                        reject(err);
                    } else {
                        resolve(json);
                    }
                });
            }
        });
    });
};

module.exports = exports;