"use strict";

var Promise = require('bluebird');
var bcrypt = require('bcrypt');
var User = require('../models').User;
var jwt = require('jsonwebtoken');
var Configurator = require('../helpers/configurator');
var FB = require('fb');

var exports = {};


exports.loginWithPassword = function(data){
    return new Promise(function(resolve, reject) {
        User.findOne({
            where:{
                email: data.email,
                provider: "email"
            }
        }).then(function (user) {
            if(!user) {
                reject({
                    name: 'Client',
                    path: 'email',
                    message: 'aria-rent.auth.error.email.notExists'
                })
            } else{
                bcrypt.compare(data.password, user.password_hash, function(err, res){
                    if(res){
                        resolve(exports.authUser(user));
                    } else {
                        reject({
                            name: 'Client',
                            path: 'password',
                            message: 'aria-rent.auth.error.password.invalid'
                        });
                    }

                });
            }
        })
    });
};

exports.getUser = function(user){
    return User.find({
        where: {
            id: user.id
        }
    });
};

exports.addUserWithAuth = function (data) {
    return exports.addUser(data).then(function(user){
        return exports.authUser(user);
    });
};

exports.updateUser = function(user, data){
    return User.updateAttributes(data, {
        where: {
            id: user.id
        }
    })
};

exports.authUser = function(user){
    return {
        token: jwt.sign(user, Configurator.get('secretKey'), {
            expiresInMinutes: 1000000
        }),
        user: user
    };
};

exports.addUser = function(data) {
    return User.create(data);
};

function socialLogin(newData){
    return User.findOrCreate({
        where: {
            vendor_id: newData.vendor_id,
            provider: newData.provider
        },
        defaults: {
            name: newData.name,
            email: newData.email,
            oauth_token: newData.oauth_token,
            vendor_id: newData.vendor_id,
            vendor_username: newData.vendor_username,
            provider: newData.provider
        }
    }).spread(function(user){
        return exports.authUser(user);
    });
}

exports.loginWithFacebook = function(token){
    return exports.getDataFromFacebook(token).then(function(data){
        return socialLogin(data);
    });
};

exports.getDataFromFacebook = function(token){
    return new Promise(function(resolve, reject){
        FB.setAccessToken(token);
        FB.api('me', { fields: ['id', 'name', 'email']}, function(res){
            if(!res || res.error || !res.email){
                reject({
                    name: 'Client',
                    path: 'oauth_token',
                    message: 'aria-rent.auth.error.oauth_token.invalid'
                });
            } else{
                resolve({
                    name: res.name,
                    email: res.email,
                    oauth_token: token,
                    oauth_secret: null,
                    provider: 'facebook',
                    vendor_id: res.id
                });
            }
        });
    });
};

module.exports = exports;