var exports = {};
var Property = require('../models').Property;
var User = require('../models').User;
var Media = require('../models').Media;
var Like = require('../models').Like;

exports.getAllValidProperties = function(user){
    var where = {};
    where.expireDate = {
        $gt: new Date()
    };
    var include = [{
        model: Media
    }, {
        model: models.Community,
        include: {
            model: models.City,
            include: {
                model: models.Country
            }
        }
    }];
    if(query.garage){
        where.garage = true;
    }
    if(user){
        include.push({
            model: Like,
            where: {
                userId: user.id
            },
            required: false
        });
    }
    return Property.findAll({
        where: where,
        include: include
    })
};

exports.deleteUser = function(user, id){
    return Like.destroy({
        where: {
            userId: user.id,
            propertyId: id
        }
    })
};

exports.getPropertiesByUser = function(user){
    return Like.findAll({
        where: {
            userId: user.id
        }
    }).then(function(likes){
        if(!likes) return [];
        return Property.findAll({
            where: {
                id: {
                    $in: likes.map(function(x){
                        return x.propertyId;
                    })
                }
            },
            include: {
                model: Media,
                limit: 1
            }
        });
    });
};

exports.addUserToProperty = function(user, propertyId){
    return Property.find({
        where: {
            id: propertyId
        },
        include: {
            model: Like,
            where: {
                userId: user.id
            },
            required: false
        }
    }).then(function(property){
        if(!property){
            throw {
                name: 'Client',
                message: 'invalid id'
            }
        }
        if(property.Like){
            throw {
                name: 'Client',
                message: 'Already Like'
            }
        }
        return Like.create({
            propertyId: property.id,
            userId: user.id
        });
    })
};

module.exports = exports;