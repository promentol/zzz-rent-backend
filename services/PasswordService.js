var exports = {};
var PasswordRecoveryRequest = require('../models').PasswordRecoveryRequest;
var User = require('../models').User;
var EmailService = require('./EmailService');
var RouteMaker = require('../helpers/routeMaker');

exports.recoverPassword = function(code, password, confirm){
    return exports.findPasswordRecoveryRequest(code).then(function(passwordRecoveryRequest){
        if(!passwordRecoveryRequest){
            throw {
                name: 'Client',
                message: 'Invalid Code'
            }
        }
        if(password !=  confirm){
            throw {
                name: 'Client',
                message: 'Password Missmatch'
            }
        }
        return passwordRecoveryRequest.User.updateAttributes({
            password: password,
            confirm: confirm
        }).then(function(){
            return PasswordRecoveryRequest.update({
                status: '1'
            }, {
                where: {
                    userId: passwordRecoveryRequest.User.id
                }
            })
        });
    });
};

exports.findPasswordRecoveryRequest = function(code){
    return PasswordRecoveryRequest.find({
        where: {
            code: code
        },
        include: User
    });
};

exports.addPasswordRequest = function(email){
    return User.find({
        where: {
            provider: 'email',
            email: email
        }
    }).then(function(user){
        if(!user){
            throw {
                name: 'Client',
                message: 'aria-rent.passwordRecovery.not_found'
            }
        }
        return  internals.generateUniqueCode().then(function(code){
            return user.createPasswordRecoveryRequest({
                createdDate: new Date(),
                code: code,
                status: 0
            }).then(function(){
                return EmailService.sendEmail({
                    email: user.email,
                    template: 'passwordRecovery',
                    link: RouteMaker.getPasswordRecoveryUrl(code),
                    name: user.name,
                    subject: 'Password Recovery'
                })
            });
        })
    });
};

var internals = {};

exports.getRequestByCode = function(code){
    return PasswordRecoveryRequest.find({
        where: {
            code: code
        }
    });
};

internals.generateUniqueCode = function(){
    var code = internals.generateCode();
    return exports.getRequestByCode(code).then(function(x){
        if(x){
            return internals.generateUniqueCode();
        } else {
            return code;
        }
    })
};

internals.generateCode = function(length){

    length = length || 6;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

module.exports = exports;