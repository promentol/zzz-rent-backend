'use strict';
module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('users', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: Sequelize.STRING,
        provider: Sequelize.STRING,
        email: Sequelize.STRING,
        password_hash: Sequelize.STRING,
        oauth_token: Sequelize.STRING,
        vendor_id: Sequelize.STRING
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};
