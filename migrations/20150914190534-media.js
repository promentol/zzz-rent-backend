'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('media', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        folder: Sequelize.STRING,
        name: Sequelize.STRING,
        extension: Sequelize.STRING
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('media');
  }
};
