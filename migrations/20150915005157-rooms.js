'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('properties', 'room', Sequelize.INTEGER );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('properties', 'room' );
  }
};
