'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.addColumn('properties', 'expireDate', Sequelize.DATE);
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('properties', 'expireDate');
  }
};
