'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('countries', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name_en: {
          type: Sequelize.INTEGER
        },
        name_ru: {
          type: Sequelize.INTEGER
        },
        name_am: {
          type: Sequelize.INTEGER
        }
      }).then(function(){
            return queryInterface.createTable('cities', {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              name_en: {
                type: Sequelize.INTEGER
              },
              name_ru: {
                type: Sequelize.INTEGER
              },
              name_am: {
                type: Sequelize.INTEGER
              },
              countryId: {
                type: Sequelize.INTEGER
              }
            }).then(function(){
              return queryInterface.addColumn('community', 'cityId', Sequelize.INTEGER);
            });
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('countries').then(function(){
        return queryInterface.dropTable('cities').then(function(){
          return queryInterface.removeColumn('community', 'cityId');
        });
      });
  }
};
