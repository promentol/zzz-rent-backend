'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('community', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name_en: {
        type: Sequelize.STRING
      },
      name_ru: {
        type: Sequelize.STRING
      },
      name_am: {
        type: Sequelize.STRING
      }
    }).then(function(){
      return queryInterface.addColumn('properties', 'communityId', Sequelize.INTEGER);
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('community').then(function(){
      return queryInterface.removeColumn('properties', 'communityId');
    })
  }
};
