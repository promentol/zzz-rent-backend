'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('users', 'settings').then(function(){
      return queryInterface.addColumn('users', 'language', Sequelize.STRING).then(function(){
        return queryInterface.addColumn('users', 'currency', Sequelize.STRING);
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'settings', Sequelize.STRING).then(function(){
      return queryInterface.removeColumn('users', 'language').then(function(){
        return queryInterface.removeColumn('users', 'currency');
      });
    });
  }
};
