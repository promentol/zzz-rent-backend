'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('mediaToProperty', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        mediaId: Sequelize.INTEGER,
        propertyId: Sequelize.INTEGER
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('mediaToProperty')
  }
};
