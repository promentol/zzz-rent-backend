'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('passwordRecoveryRequest', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        code: Sequelize.STRING,
        status: Sequelize.INTEGER,
        createdDate: Sequelize.DATE,
        usedDate: Sequelize.DATE,
        userId: Sequelize.INTEGER
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('passwordRecoveryRequest');
  }
};
