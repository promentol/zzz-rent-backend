'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('likeList', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true
        },
        propertyId: {
          type: Sequelize.INTEGER
        },
        userId: {
          type: Sequelize.INTEGER
        }
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('likeList');
  }
};
