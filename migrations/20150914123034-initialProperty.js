'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('properties', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        latitude: {
          type: Sequelize.FLOAT
        },
        longitude: {
          type: Sequelize.FLOAT
        },
        price: {
          type: Sequelize.INTEGER
        },
        currency: {
          type: Sequelize.STRING
        },
        purpose: Sequelize.STRING,
        type: Sequelize.STRING,
        subType: Sequelize.STRING,
        /**
         *  FOR HOUSE SALE
         */
        buildingType: Sequelize.STRING,
        buildingYear: Sequelize.INTEGER,
        community: Sequelize.INTEGER,
        street_am: Sequelize.STRING,
        street_ru: Sequelize.STRING,
        street_en: Sequelize.STRING,
        area: Sequelize.INTEGER,
        bathroom: Sequelize.INTEGER,
        floor: Sequelize.INTEGER,
        elevator: Sequelize.BOOLEAN,
        balcony: Sequelize.BOOLEAN,
        cellar: Sequelize.BOOLEAN,
        loft: Sequelize.BOOLEAN,
        garage: Sequelize.BOOLEAN,
        quality: Sequelize.INTEGER,
        ceiling: Sequelize.INTEGER,
        flooring: Sequelize.STRING,
        /* House rental */
        internet: Sequelize.BOOLEAN,
        cableTV: Sequelize.BOOLEAN,
        furniture: Sequelize.BOOLEAN,
        /* Commercial  */
        entrance: Sequelize.STRING,
        parking: Sequelize.STRING,
        amenity: Sequelize.INTEGER,
        height: Sequelize.INTEGER,
        isPrivate: Sequelize.BOOLEAN,
        description: Sequelize.STRING,
        zip: Sequelize.STRING
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('properties');
  }
};
