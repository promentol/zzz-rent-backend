"use strict";
var RouteMaker = require('../helpers/routeMaker');
module.exports = function(sequelize, DataTypes){
    var City = sequelize.define('City', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name_en: {
            type: DataTypes.STRING
        },
        name_am: {
            type: DataTypes.STRING
        },
        name_ru: {
            type: DataTypes.STRING
        },
        countryId: {
            type: DataTypes.INTEGER
        }
    }, {
        tableName: 'countries',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                City.hasMany(models.Community, {
                    foreignKey: 'cityId'
                });
                City.belongsTo(models.Country, {
                    foreignKey: 'countryId'
                });
            }
        },
        instanceMethods: {
            toJSON: function(){
                return {
                    id: this.id,
                    name_en: this.name_en,
                    name_am: this.name_am,
                    name_ru: this.name_ru,
                    country: this.Country
                };
            }
        }
    });
    return City;
};