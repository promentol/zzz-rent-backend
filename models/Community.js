"use strict";
var RouteMaker = require('../helpers/routeMaker');
module.exports = function(sequelize, DataTypes){
    var Community = sequelize.define('Community', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name_en: {
            type: DataTypes.STRING
        },
        name_am: {
            type: DataTypes.STRING
        },
        name_ru: {
            type: DataTypes.STRING
        },
        cityId: {
            type: DataTypes.INTEGER
        }
    }, {
        tableName: 'community',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                Community.belongsTo(models.City, {
                    foreignKey: 'cityId'
                });
            }
        },
        instanceMethods: {
            toJSON: function(){
                return {
                    name_en: this.name_en,
                    name_am: this.name_am,
                    name_ru: this.name_ru,
                    city: this.City
                };
            }
        }
    });
    return Community;
};