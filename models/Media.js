"use strict";

var RouteMaker = require('../helpers/routeMaker');

module.exports = function(sequelize, DataTypes){
    var Media = sequelize.define('Media', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        },
        extension: {
            type: DataTypes.STRING
        },
        folder: {
            type: DataTypes.STRING
        }
    }, {
        tableName: 'media',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                Media.belongsToMany(models.Property, {
                    through: 'mediaToProperty',
                    foreignKey: 'mediaId',
                    otherKey: 'propertyId'
                })
            }
        },
        instanceMethods: {
            toJSON: function(){
                return RouteMaker.getMediaUrl(this);
            }
        }
    });
    return Media;
};