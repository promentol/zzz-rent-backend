"use strict";

module.exports = function(sequelize, DataTypes){
    var PasswordRecoveryRequest = sequelize.define('PasswordRecoveryRequest', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        code: {
            type: DataTypes.STRING
        },
        status: {
            //0 open
            //1 used
            //2 canceled
            //3 timeout
            type: DataTypes.INTEGER
        },
        createdDate: {
            type: DataTypes.DATE
        },
        usedDate: {
            type: DataTypes.DATE
        },
        userId: {
            type: DataTypes.INTEGER
        }
    }, {
        tableName: 'passwordRecoveryRequest',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                PasswordRecoveryRequest.belongsTo(models.User, {
                    foreignKey: 'userId'
                });
            }
        },
        instanceMethods: {
        }
    });
    return PasswordRecoveryRequest;
};