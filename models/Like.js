"use strict";

var RouteMaker = require('../helpers/routeMaker');

module.exports = function(sequelize, DataTypes){
    var Like = sequelize.define('Like', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        propertyId: {
            type: DataTypes.INTEGER
        },
        userId: {
            type: DataTypes.INTEGER
        }
    }, {
        tableName: 'likeList',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                Like.belongsTo(models.Property, {
                    foreignKey: 'propertyId'
                });
                Like.belongsTo(models.User, {
                    foreignKey: 'userId'
                });
            }
        },
        instanceMethods: {
            toJSON: function(){
                return true;
            }
        }
    });
    return Like;
};