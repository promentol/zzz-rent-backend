"use strict";
var RouteMaker = require('../helpers/routeMaker');
module.exports = function(sequelize, DataTypes){
    var Country = sequelize.define('Country', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name_en: {
            type: DataTypes.STRING
        },
        name_am: {
            type: DataTypes.STRING
        },
        name_ru: {
            type: DataTypes.STRING
        }
    }, {
        tableName: 'countries',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                Country.hasMany(models.City, {
                    foreignKey: 'countryId'
                });
            }
        },
        instanceMethods: {
            toJSON: function(){
                return {
                    id: this.id,
                    name_en: this.name_en,
                    name_am: this.name_am,
                    name_ru: this.name_ru
                };
            }
        }
    });
    return Country;
};