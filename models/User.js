"use strict";

var i18n = require('i18n');
var bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes){

    var User = sequelize.define("User", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: {
                    msg: 'aria-rent.users.error.name.notEmpty'
                }
            }
        },
        provider: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: {
                    msg: 'aria-rent.users.error.email.notEmpty'
                },
                isEmail: {
                    msg: 'aria-rent.users.error.email.invalidEmail'
                },
                isUniquePerProvider: function(email, next){
                    var self = this;
                    if(self.provider === "email"){
                        User.find({
                            where: {
                                email: email,
                                provider: "email"
                            }
                        }).then(function(user){
                            if(user != null && (self.id == null || self.id != user.id)){
                                return next('aria-rent.users.error.email.alreadyExists');
                            }
                            next();
                        });
                    } else {
                        next();
                    }
                }
            }
        },
        password: {
            type: DataTypes.VIRTUAL,
            set: function(val){
                val = val || "";
                this.setDataValue('password',val);
                var salt = bcrypt.genSaltSync(10);
                this.setDataValue('password_hash', bcrypt.hashSync(val, salt))
            },
            validate: {
                notEmpty: {
                    msg: 'aria-rent.users.error.password.notEmpty'
                },
                len: {
                    args: [4, 31],
                    msg: 'aria-rent.users.error.password.length'
                }
            }
        },
        confirm: {
            type: DataTypes.VIRTUAL,
            validate: {
                match: function(confirm, next){
                    if(this.password != confirm) {
                        next('aria-rent.users.error.password.mismatch');
                    } else {
                        next();
                    }
                }
            }
        },
        password_hash: {
            type: DataTypes.STRING
        },
        oauth_token: {
            type: DataTypes.STRING
        },
        vendor_id: {
            type: DataTypes.STRING
        },
        language: {
            type: DataTypes.STRING,
            validate: {
                isIn: [['en', 'ru', 'am']]
            }

        },
        currency: {
            type: DataTypes.STRING,
            validate: {
                isIn: [['usd', 'rub', 'amd']]
            }
        }
    }, {
        tableName: 'users',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                User.hasMany(models.PasswordRecoveryRequest, {
                    foreignKey: 'userId'
                });
                User.belongsToMany(models.Property, {
                    through: 'likeList',
                    foreignKey: 'userId',
                    otherKey: 'propertyId'
                })
            }
        },
        instanceMethods: {
            toJSON: function(){
                return {
                    id: this.id,
                    vendor_id: this.vendor_id,
                    name: this.name,
                    email: this.email,
                    oauth_token: this.oauth_token,
                    provider: this.provider,
                    language: this.language,
                    currency: this.currency
                }
            }
        }
    });

    return User;
};