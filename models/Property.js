"use strict";

var i18n = require('i18n');

module.exports = function(sequelize, DataTypes){

    var Property = sequelize.define("Property", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        latitude: {
            type: DataTypes.FLOAT
        },
        longitude: {
            type: DataTypes.FLOAT
        },
        price: {
            type: DataTypes.INTEGER
        },
        currency: {
            type: DataTypes.STRING,
            validate: {
                isIn: [['usd', 'rub', 'amd']]
            }
        },
        expireDate: {
            type: DataTypes.DATE
        },
        buildingType: DataTypes.STRING,
        buildingYear: DataTypes.INTEGER,
        communityId: DataTypes.INTEGER,
        street_am: DataTypes.STRING,
        street_ru: DataTypes.STRING,
        street_en: DataTypes.STRING,
        area: DataTypes.INTEGER,
        room: DataTypes.INTEGER,
        bathroom: DataTypes.INTEGER,
        floor: DataTypes.INTEGER,
        elevator: DataTypes.BOOLEAN,
        balcony: DataTypes.BOOLEAN,
        cellar: DataTypes.BOOLEAN,
        loft: DataTypes.BOOLEAN,
        garage: DataTypes.BOOLEAN,
        quality: DataTypes.INTEGER,
        ceiling: DataTypes.INTEGER,
        flooring: DataTypes.STRING,
        /* House rental */
        internet: DataTypes.BOOLEAN,
        cableTV: DataTypes.BOOLEAN,
        furniture: DataTypes.BOOLEAN,
        /* Commercial  */
        entrance: DataTypes.STRING,
        parking: DataTypes.STRING,
        amenity: DataTypes.INTEGER,
        height: DataTypes.INTEGER,
        isPrivate: DataTypes.BOOLEAN,
        description: DataTypes.STRING,
        zip: DataTypes.STRING
    }, {
        tableName: 'properties',
        timestamps: false,
        freezeTableName: true,
        classMethods: {
            associate: function(models) {
                Property.belongsToMany(models.Media, {
                    through: 'mediaToProperty',
                    foreignKey: 'propertyId',
                    otherKey: 'mediaId'
                });

                Property.hasOne(models.Like, {
                    foreignKey: 'propertyId'
                });

                Property.belongsTo(models.Community, {
                    foreignKey: 'communityId'
                });
            }
        },
        instanceMethods: {
            toJSON: function(){
                return {
                    id: this.id,
                    latitude: this.latitude,
                    longitude: this.longitude,
                    price: this.price,
                    currency: this.currency,
                    buildingType: this.buildingType,
                    buildingYear: this.buildingYear,
                    community: this.Community,
                    street_am: this.street_am,
                    street_ru: this.street_ru,
                    street_en: this.street_en,
                    area: this.area,
                    bathroom: this.bathroom,
                    room: this.room,
                    floor: this.floor,
                    elevator: this.elevator,
                    balcony: this.balcony,
                    cellar: this.cellar,
                    loft: this.loft,
                    garage: this.garage,
                    quality: this.quality,
                    ceiling: this.ceiling,
                    flooring: this.flooring,
                    /* House rental */
                    internet: this.internet,
                    cableTV: this.cableTV,
                    furniture: this.furniture,
                    /* Commercial  */
                    entrance: this.entrance,
                    parking: this.parking,
                    amenity: this.amenity,
                    height: this.height,
                    isPrivate: this.isPrivate,
                    description: this.description,
                    zip: this.zip,
                    medias: this.Media,
                    like: this.Like,
                    shareLink: 'http://aria-rent.am/asd/asd'
                }
            }
        }
    });

    return Property;
};