"use strict";

var express = require('express');
var router = express.Router();
var PasswordService = require('../../services/PasswordService');

router.post('/', function(req, res, next){
    PasswordService.addPasswordRequest(req.body.email).then(function(){
        res.send({});
    }).catch(next);
});

router.post('/verify', function(req, res, next){
    PasswordService.getRequestByCode(req.body.code).then(function(x){
        if(x){
            res.send({});
        } else {
            next({
                name: 'Client',
                message: 'Invalid Key'
            })
        }
    }).catch(next);
});

router.post('/recover', function(req, res, next){
    if(!req.body.code){
        next({
            name: 'Client',
            message: 'Invalid Code'
        })
    } else {
        PasswordService.recoverPassword(req.body.code, req.body.password, req.body.confirm).then(function(){
            res.send({});
        }).catch(next);
    }
});

module.exports = router;