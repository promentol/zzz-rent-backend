"use strict";

var express = require('express');
var router = express.Router();
var PropertyService = require('../../services/PropertyService');

router.get('/', function(req, res, next){
    PropertyService.getAllValidProperties(req.user, req.query).then(function(properties){
        res.send(properties);
    }).catch(next);
});

module.exports = router;