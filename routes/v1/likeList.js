"use strict";

var express = require('express');
var router = express.Router();
var UserService = require('../../services/UserService');
var PropertyService = require('../../services/PropertyService');

router.get('/', function(req, res, next){
    if(!req.user){
        next({
            name: 'Unauth'
        })
    } else {
        PropertyService.getPropertiesByUser(req.user).then(function(properties){
            res.send(properties);
        });
    }
});

router.post('/', function(req, res, next){
    if(!req.user){
        next({
            name: 'Unauth'
        })
    } else {
        PropertyService.addUserToProperty(req.user, req.body.propertyId).then(function(){
            res.send({});
        }).catch(next);
    }
});

router.delete('/:id', function(req, res, next){
    if(!req.user){
        next({
            name: 'Unauth'
        })
    } else {
        PropertyService.deleteUser(req.user, req.params.id).then(function(){
            res.send({});
        }).catch(next);
    }
});

module.exports = router;