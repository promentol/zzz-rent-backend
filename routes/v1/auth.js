"use strict";

var express = require('express');
var router = express.Router();
var UserService = require('../../services/UserService');

router.get('/', function(req, res, next){
    if(!req.user){
        next({
            name: 'Unauth'
        })
    } else {
        UserService.getUser(req.user).then(function(user){
            return UserService.authUser(user)
        }).then(function(user){
            res.send(user);
        });
    }
});

router.post('/', function(req, res, next){
    var promise;
    if(req.body.provider == 'email'){
        promise = UserService.loginWithPassword({
            email: req.body.email || "",
            password: req.body.password || ""
        });
    } else if(req.body.provider == 'facebook') {
        promise = UserService.loginWithFacebook(req.body.oauth_token);
    }

    if(promise){
        promise.then(function(x){
            res.send(x);
        }).catch(next);
    } else{
        next({
            name: 'Client',
            path: 'provider',
            message: 'aria-rent.users.error.provider.invalid'
        });
    }
});

router.post('/user', function(req, res, next){
    UserService.addUserWithAuth({
        email: req.body.email || "",
        name: req.body.name || "",
        password: req.body.password,
        confirm: req.body.confirm || "",
        provider: 'email',
        language: req.body.language || "en",
        currency: req.body.currency || "usd"
    }).then(function(auth){
        res.status(201).send(auth);
    }).catch(function(err){
        next(err);
    });
});

router.put('/me', function(req, res, next){
    if(!req.user){
        next({
            name: 'Unauth'
        });
    } else {
        UserService.updateUser(req.user, {
            name: req.body.name,
            settings: req.body.language,
            currency: req.body.currency
        }).then(function(){
            res.status(200).send({});
        }).catch(function(err){
            next(err);
        });
    }
});

module.exports = router;