"use strict";

var Configurator = require('../helpers/configurator');
var jwt = require('jsonwebtoken');

module.exports = function(){
    return function (req, res, next) {
        req.isApi = req.originalUrl.indexOf('/api/') >= 0;
        req.user = jwt.verify(req.get('authtoken'), Configurator.get('secretKey'), function(err, data){
            req.user = data;
            next();
        })
    }
};