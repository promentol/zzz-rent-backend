"use strict";

module.exports = function(){
    return function (err, req, res, next) {
        console.log(err);
        if(err.name && err.name == "SequelizeValidationError"){
            res.status(400).send({
                errors: err.errors.map(function(x){
                            return {
                                path: x.path,
                                message: res.__(x.message)
                            }
                        })
            });
        } else if(err.name == "Client"){
            res.status(400).send({
                errors: [
                    {
                        path: err.path,
                        message: res.__(err.message)
                    }
                ]
            });
        } else if(err.name == 'Unauth'){
           res.status(403).send({
               "errors": [
                   {
                       path: "request",
                       message: res.__('aria-rent.http.error.403')
                   }
               ]
           })
        } else if(err.status == 404){
            res.status(404).send({
                "errors": [
                    {
                        path: "request",
                        message: res.__('aria-rent.http.error.404')
                    }
                ]
            })
        } else {
            res.status(500).send({
                "errors": [
                    {
                        path: "server",
                        message: res.__('aria-rent.http.error.500')
                    }
                ]
            })
        }
    }
};