var env = process.env.NODE_ENV || "development";
var config = require(__dirname + '/../config/config')[env];

module.exports = {
    get: function (key) {
        return config[key];
    }
};