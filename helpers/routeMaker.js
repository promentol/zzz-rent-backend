"use strict";

var Configurator = require('../helpers/configurator');

var exports = {};

exports.getPasswordRecoveryUrl = function(code){
    return this.makeUrl('/passwordRecovery/'+code);
};

exports.makeUrl = function(x){
    return Configurator.get('baseUrl') + x;
};

exports.getMediaUrl = function(media){
    return exports.makeUrl('/images/'+media.folder+'/'+media.name+'.'+media.extension)
};

module.exports = exports;