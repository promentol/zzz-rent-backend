"use strict";

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var apiMiddle = require('./middleware/api');
var errorReporter = require('./middleware/error');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var i18n = require('i18n');
var compression = require('compression');

i18n.configure({
    locales: ['en'],
    defaultLocale: 'en',
    directory: __dirname + '/locales'
});

var routes = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, authtoken, Content-Type");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    if ('OPTIONS' == req.method) {
        res.sendStatus(204);
    }
    else {
        next();
    }
});

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(i18n.init);
app.use(express.static(path.join(__dirname, 'public')));

app.use(apiMiddle());
app.use(compression());

app.use('/', routes);

var auth = require('./routes/v1/auth');
var passwordRecovery = require('./routes/v1/passwordRecovery');
var property = require('./routes/v1/property');
var likeList = require('./routes/v1/likeList');

app.use('/api/v1/auth', auth);
app.use('/api/v1/passwordRecovery', passwordRecovery);
app.use('/api/v1/property', property);
app.use('/api/v1/likeList', likeList);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

app.use(errorReporter());

module.exports = app;
